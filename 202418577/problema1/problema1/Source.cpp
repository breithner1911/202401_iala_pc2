#include <iostream>
#include <cmath>
#include "upc.h" 

using namespace std;

bool Primo(int* num) {
    if (*num <= 1) {
        return false;
    }

bool Divisible = false;

    if (*num % 2 == 0) {
        gotoxy(10, 2); cout << *num << " es divisible por 2.\n";
        Divisible = true;
    }
    
    if (*num % 3 == 0) {
        gotoxy(10, 3); cout << *num << " es divisible por 3.\n";
        Divisible = true;
    }
    
    if (*num % 4 == 0) {
        gotoxy(10, 4); cout << *num << " es divisible por 4.\n";
        Divisible = true;
    }
    
    if (*num % 5 == 0) {
        gotoxy(10, 5); cout << *num << " es divisible por 5.\n";
        Divisible = true;
    }
    
    if (*num % 6 == 0) {
        gotoxy(10, 6); cout << *num << " es divisible por 6.\n";
        Divisible = true;
    }

    if (Divisible) {
        return false;
    }

   
    int limite = static_cast<int>(sqrt(*num));
    for (int i = 7; i <= limite; i += 6) {
        if (*num % i == 0 || *num % (i + 2) == 0) {
            return false;
        }
    }

    return true;
}

int main() {
    int numero;
    gotoxy(10, 0); cout << "Ingrese un n�mero: ";
    cin >> numero;

    if (Primo(&numero)) {
        gotoxy(10, 1); cout << numero << " es un n�mero primo.\n";
    }
    else {
        gotoxy(10, 1); cout << numero << " no es un n�mero primo.\n";
    }
    system("pause");
    return 0;
}


